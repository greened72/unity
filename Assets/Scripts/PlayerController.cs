﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {


    public float Speed;
    public Text countText;

    private int count;
    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetcountText();
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetcountText();
        }
    }
    private void FixedUpdate()
    {

       
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0,moveVertical);
        rb.AddForce(movement * Speed);
      
    }

    void SetcountText()
    {
        countText.text = "Count: " + count.ToString(); 
    }
}
